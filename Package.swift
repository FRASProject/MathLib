// swift-tools-version:5.7
import PackageDescription

let package = Package(
  name: "MathLib",
  platforms: [.macOS(.v13)],
  products: [
    .library(
      name: "MathLib",
      targets: ["MathLib"]),
  ],
  dependencies: [
    .package(
      url: "https://github.com/apple/swift-collections.git",
      .upToNextMajor(from: "1.0.0")
    ),
    .package(url: "https://github.com/apple/swift-algorithms", from: "1.0.0"),
  ],
  targets: [
    .target(
      name: "MathLib",
      dependencies: [
        .product(name: "Collections", package: "swift-collections"),
        .product(name: "Algorithms", package: "swift-algorithms"),
      ]),
    .testTarget(
      name: "MathLibTests",
      dependencies: ["MathLib"]),
  ]
)

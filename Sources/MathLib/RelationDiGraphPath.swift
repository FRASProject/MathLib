//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/10/21.
//

import Foundation

public struct RelationDiGraphPath {
  public var graph: IntDiGraph
  public let reflexive: Bool
  public let ireflexive: Bool
  public let transitiveClosure: Bool
  public var elements: [Int] {
    graph.diVertices.map {$0.value.id}
  }
  public init(
    graph: IntDiGraph,
    reflexive: Bool = false,
    ireflexive: Bool = false,
    transitiveClosure: Bool = false)
  {
    self.graph = graph
    self.reflexive = reflexive
    self.ireflexive = ireflexive
    self.transitiveClosure = transitiveClosure
  }
  public func isARelatedToB(a: Int, b: Int) -> Bool {
    if a == b {
      if reflexive {
        return true
      } else if ireflexive {
        return false
      } else {
        precondition(!graph.isEdge(from: a, to: b), "Debug, remove this")
        return graph.isEdge(from: a, to: b)
      }
    }
    if transitiveClosure {
      return graph.isTherePath(from: a, to: b)
    } else {
      return graph.isEdge(from: a, to: b)
    }
  }
}

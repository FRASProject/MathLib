//
//  combinatorics.swift
//  SwiftMath
//
//  Created by Pavel R on 18/05/2018.
//

import Foundation
import Algorithms


/// Generates all k-element subsets of a given set. Use Algorithms library instead.
/// - Parameters:
///   - set: Set
///   - size: size of subsets.
/// - Returns: All k-elements subsets.
@available(*, deprecated)
public func generateAllSubsets<T: Hashable>(of set: Set<T>, size: Int) -> Set<Set<T>> {
  // FIXME: Write tests for this function. It probably contains a bug.
  if size == 0 {
    return [Set<T>()]
  }
  if size > set.count {
    return []
  }
  if size == set.count {
    return [set]
  }
  
  let element = set.first!
  var setWithoutElement = set
  setWithoutElement.remove(element)
  
  let subsetsWithout = generateAllSubsets(of: setWithoutElement, size: size)
  let subsetsWith = generateAllSubsets(of: setWithoutElement, size: size - 1)
  
  var returnedSet = Set<Set<T>>()
  for var subset in subsetsWith {
    subset.insert(element)
    returnedSet.insert(subset)
  }
  return returnedSet.union(subsetsWithout)
}

public func permutationsWithRepetitionNG<C: Collection>(of a: C, size: Int) ->
PermutationsSequence<CycledTimesCollection<C>>
{
  a.cycled(times: size).permutations(ofCount: size)
}

@available(*, deprecated)
public func permutationsWithRepetition<T>(of a: [T], size: Int) -> [[T]] {
  if size == 0 {
    return [[]]
  }
  var result = [[T]]()
  let permutationsSizeMinusOne = permutationsWithRepetition(of: a, size: size - 1)
  for element in a {
    for permutationSizeMinusOne in permutationsSizeMinusOne {
      result.append(permutationSizeMinusOne + [element])
    }
  }
  return result
}

//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/15/21.
//

import Foundation

public protocol SimulatedAnnealing {
  associatedtype State
  var initState: State { get }
  func cost(of state: State) -> Double?
  func randomNextState(from state: State) -> State?
}

extension SimulatedAnnealing {
  public func startSimulatedAnnealing(
    startTemperature: Double,
    temperatureStep: Double,
    constantK: Double = 2.1,
    iterationCallback: ((State, Double, Int)-> ())? = nil,
    verbose: Bool = false) -> State
  {
    precondition(startTemperature >= 0)
    precondition(temperatureStep >= 0)
    precondition(startTemperature > temperatureStep)
    if verbose { print("Starting simulatedAnnealing. Starting temperature \(startTemperature)")}
    var iteration = 0
    var state = initState
    guard let initStateCost = cost(of: state) else {
      fatalError("Cannot compute init state cost.")
    }
    var stateCost = initStateCost
    var temperature = startTemperature
    while temperature >= 0 {
      guard let nextState = randomNextState(from: state) else { return state }
      guard let nextStateCost = cost(of: nextState) else { continue }
      let delta = nextStateCost - stateCost
      if delta < 0 {
        state = nextState
        stateCost = nextStateCost
      } else {
        let changeStateProbability = exp(-delta / (temperature * constantK))
        if Double.random(in: 0..<1) < changeStateProbability {
          state = nextState
          stateCost = nextStateCost
        }
      }
      if verbose && iteration % 100 == 0 {
        print("Current cost: \(stateCost) Temperature: \(Int(temperature.rounded())) Iteration: \(iteration)")
      }
      iteration += 1
      temperature -= temperatureStep
      if let iterationCallback = iterationCallback {
        iterationCallback(state, temperature, iteration)
      }
    }
    if verbose { print("Finished simulated annealing. Final state cost: \(stateCost)")}
    return state
  }
}

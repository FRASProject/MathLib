//
//  ArrayUtils.swift
//  MathLib
//
//  Created by Pavel Rytir on 7/21/18.
//

import Foundation

public enum ArrayUtils {
  public static func findFirstIndexOfDifferentElement<T: Equatable>(_ a: [T], _ b: [T]) -> Int {
    for i in 0..<min(a.count, b.count) {
      if a[i] != b[i] {
        return i
      }
    }
    return min(a.count, b.count)
  }
}

public enum DictionaryUtils {
  public static func cartesianProduct<T>(
    _ a: [[String: T]],
    _ b: [[String: T]]) -> [[String: T]]
  {
    var result = [[String: T]]()
    for da in a {
      for db in b {
        var newDict = da
        newDict.merge(db, uniquingKeysWith: { a, b in a })
        result.append(newDict)
      }
    }
    return result
  }
}

public enum VectorUtils {
  /// Subtracts two array
  /// - Parameters:
  ///   - a:
  ///   - b:
  /// - Returns: for all i result[i] = a[i] - b[i]
  public static func subtractArrays<N: AdditiveArithmetic>(a: [N], b: [N]) -> [N] {
    assert(a.count == b.count)
    var result = [N]()
    result.reserveCapacity(a.count)
    for i in a.indices {
      result.append(a[i] - b[i])
    }
    return result
  }
  public static func addArrays<N: AdditiveArithmetic>(a: [N], b: [N]) -> [N] {
    assert(a.count == b.count)
    var result = [N]()
    result.reserveCapacity(a.count)
    for i in a.indices {
      result.append(a[i] + b[i])
    }
    return result
  }
  public static func addArraysWithDifferentSizes<N: AdditiveArithmetic>(a: [N], b: [N]) -> [N] {
    var result = a.count < b.count ? b : a
    let adding = a.count < b.count ? a : b
    for i in adding.indices {
      result[i] += adding[i]
    }
    return result
  }
  public static func multiply<N: Numeric>(_ x: [N], by scalar: N) -> [N] {
    var result = x
    for i in result.indices {
      result[i] *= scalar
    }
    return result
  }
  public static func divide<N: BinaryInteger>(_ x: [N], by scalar: N) -> [N] {
    var result = x
    for i in result.indices {
      result[i] /= scalar
    }
    return result
  }
  public static func divide<N: FloatingPoint>(_ x: [N], by scalar: N) -> [N] {
    var result = x
    for i in result.indices {
      result[i] /= scalar
    }
    return result
  }
}


/// Generates all subsets of a
/// - Parameter a:
/// - Returns: Array of all subsets.
public func generateAllSubsets<E>(of a: [E]) -> [[E]] {
  var result = [[E]]()
  result.reserveCapacity(1 << a.count)
  func add(_ t: [E], idx: Int) {
    if idx >= a.count {
      result.append(t)
    } else {
      add(t, idx: idx + 1)
      var t = t
      t.append(a[idx])
      add(t, idx: idx + 1)
    }
  }
  add([], idx: 0)
  return result
}


public func generateCartesianProduct<K: Hashable, V>(of dict:[K:[V]]) -> [[K:V]] {
  var combinations = [[K:V]]()
  let keys = Array(dict.keys)
  func generate(keyIdx: Int, combinationDict: [K:V]) {
    if keyIdx >= keys.count {
      combinations.append(combinationDict)
    } else {
      if dict[keys[keyIdx]]!.isEmpty {
        generate(keyIdx: keyIdx + 1, combinationDict: combinationDict)
      } else {
        for v in dict[keys[keyIdx]]! {
          var newCombinationDict = combinationDict
          newCombinationDict[keys[keyIdx]] = v
          generate(keyIdx: keyIdx + 1, combinationDict: newCombinationDict)
        }
      }
    }
  }
  var initDict = [K:V]()
  initDict.reserveCapacity(keys.count)
  generate(keyIdx: 0, combinationDict: initDict)
  return combinations
}

/// Generates cartesion product of n sets.
/// - Parameter sets: An array of sets. Each set is represented as tuple (set_index, array of elements)
/// - Returns: An array that contains elements. Each element is an array with tuples (set_index, element).
public func generateCartesianProduct<E>(of sets: [(Int,[E])]) ->[[(Int,E)]] {
  var combinations = [[(Int,E)]]()
  func generate(keyIdx: Int, combination: [(Int,E)]) {
    if keyIdx >= sets.count {
      if !combination.isEmpty {
        combinations.append(combination)
      }
    } else {
      if sets[keyIdx].1.isEmpty {
        generate(keyIdx: keyIdx + 1, combination: combination)
      } else {
        for v in sets[keyIdx].1 {
          var newCombination = combination
          newCombination.append((sets[keyIdx].0, v))
          generate(keyIdx: keyIdx + 1, combination: newCombination)
        }
      }
    }
  }
  var initArray = [(Int,E)]()
  initArray.reserveCapacity(sets.count)
  generate(keyIdx: 0, combination: initArray)
  return combinations
}

extension Array {
  public mutating func popAndRevert(last: Int) -> [Element] {
    var last = last
    var result = Self()
    while last > 0, let lastElement = self.popLast() {
      result.append(lastElement)
      last -= 1
    }
    result.reverse()
    return result
  }
  public func removingElement(at index: Int) -> Self {
    var result = Self()
    result.append(contentsOf: self[..<index])
    result.append(contentsOf: self[(index + 1)...])
    return result
  }
}

extension Array where Element : Hashable {
  public func removingDuplicates() -> Self {
    var processed = Set<Element>()
    var result = Self()
    for e in self {
      if !processed.contains(e) {
        result.append(e)
        processed.insert(e)
      }
    }
    return result
  }
  public func subtracting(_ s: Set<Element>) -> Self {
    filter {!s.contains($0)}
  }
  public func indices(of elements: [Element]) -> [Int] {
    //TODO: Optimize this
    elements.map {
      self.firstIndex(of: $0)!
    }
    
    
//    var indices = [Int]()
//    for (i, e) in self.enumerated() where elements.contains(e) {
//      indices.append(i)
//    }
//    precondition(elements.count == indices.count)
//    return indices
  }
}

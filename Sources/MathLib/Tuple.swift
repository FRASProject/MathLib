//
//  Tuple.swift
//  MathLib
//
//  Created by Pavel Rytir on 8/25/18.
//

import Foundation

public struct Tuple<S,T> {
  public init(_ first: S, _ second: T) {
    self.first = first
    self.second = second
  }
  public var first: S
  public var second: T
}

extension Tuple: Equatable where T: Equatable, S: Equatable {}

extension Tuple: Hashable where T: Hashable, S: Hashable {}

extension Tuple: Codable where T: Codable, S: Codable {}

public struct Triple<R,S,T> {
  public var first: R
  public var second: S
  public var third: T
  public init(_ first: R, _ second: S, _ third: T) {
    self.first = first
    self.second = second
    self.third = third
  }
}

extension Triple: Equatable where R: Equatable, S: Equatable, T: Equatable {}

extension Triple: Hashable where R: Hashable, S: Hashable, T: Hashable {}

extension Triple: Codable where R: Codable, S: Codable, T: Codable {}


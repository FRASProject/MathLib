//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/10/21.
//

import Foundation

extension FiniteDiGraph {
  public func findAllVerticesInTheSameComponent(as vertexId: V.Index) -> Set<V.Index> {
    var component = [V.Index]()
    breadthFirstSearch(from: vertexId, callback: { (vertex) -> Bool in
      component.append(vertex.id)
      return true
    })
    return Set(component)
  }
  
  public func isTherePath(from start: V.Index, to end: V.Index) -> Bool {
    var pathExists = false
    breadthFirstSearch(from: start, callback: {
      if $0.id == end {
        pathExists = true
        return false
      }
      return true
    })
    return pathExists
  }
  
  public func areInTheSameComponent(vertices: [V.Index]) -> Bool {
    guard vertices.count > 1 else {
      return true
    }
    let start = vertices.first!
    var unvisited = Set(vertices[1...])
    breadthFirstSearch(from: start, callback: { (vertex) -> Bool in
      unvisited.remove(vertex.id)
      return !unvisited.isEmpty
    })
    return unvisited.isEmpty
  }
  
  public func areInTheSameCompanentAndContainAtLeastOne(
    vertices: [V.Index],
    atLeastOneVertices: [V.Index]) -> Bool
  {
    guard vertices.count > 1 else {
      return true
    }
    let start = vertices.first!
    var unvisited = Set(vertices[1...])
    let atLeastOneVerticesSet = Set(atLeastOneVertices)
    var visited = false
    breadthFirstSearch(from: start, callback: { (vertex) -> Bool in
      unvisited.remove(vertex.id)
      if atLeastOneVerticesSet.contains(vertex.id) {
        visited = true
      }
      return !(unvisited.isEmpty && visited)
    })
    return unvisited.isEmpty && visited
  }
}

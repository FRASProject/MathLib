//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/10/21.
//

import Foundation


extension FiniteGraph {
  public func edgesOfMinimalSpanningTreeKruskal(
    lengths: (E.Index) -> Double) -> [E.Index]
  {
    var edgesInSpanningTree = [E.Index]()
    let sortedEdges = Array(edges.map {$0.0}).sorted(by: {lengths($0) < lengths($1)})
    var unionFind = UnionFindWeightedQuickUnionPathCompression<V.Index>()
    for (vertexId,_) in vertices {
      unionFind.addSetWith(vertexId)
    }
    for edgeId in sortedEdges {
      let edge = edge(edgeId)!
      let v1 = edge.vertex1Id
      let v2 = edge.vertex2Id
      if !unionFind.inSameSet(v1, and: v2) {
        edgesInSpanningTree.append(edgeId)
        unionFind.unionSetsContaining(v1, and: v2)
      }
    }
    return edgesInSpanningTree
  }
}

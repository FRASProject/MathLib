//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/10/21.
//

import Foundation

extension FiniteDiGraph {
  public func findSomeMaximalSubForest() -> DiGraph<V,E> {
    var uncheckedVertices = Set<V.Index>(diVertices.map {$0.key})
    var forestEdges = Set<E.Index>()
    while !uncheckedVertices.isEmpty {
      var lifo = [V.Index]()
      var visited = Set<V.Index>()
      let firstAvailable = uncheckedVertices.first!
      visited.insert(firstAvailable)
      lifo.append(firstAvailable)
      uncheckedVertices.remove(firstAvailable)
      while let topVertexId = lifo.popLast() {
        for neigborhEdgeVertex in outgoingNeighbors(of: topVertexId) {
          if !visited.contains(neigborhEdgeVertex.vertex.id) {
            let neighborhId = neigborhEdgeVertex.vertex.id
            let forwardEdgeId = neigborhEdgeVertex.edge.id
            visited.insert(neighborhId)
            lifo.append(neighborhId)
            uncheckedVertices.remove(neighborhId)
            forestEdges.insert(forwardEdgeId)
          }
        }
      }
    }
    return DiGraph(diVertices: diVertices, diEdges: diEdges.filter {forestEdges.contains($0.key)})
  }
  
  public func isCycleFree() -> Bool {
    var uncheckedVertices = Set<V.Index>(diVertices.map {$0.key})
    while !uncheckedVertices.isEmpty {
      var lifo = [V.Index]()
      var visited = Set<V.Index>()
      let firstAvailable = uncheckedVertices.first!
      visited.insert(firstAvailable)
      lifo.append(firstAvailable)
      uncheckedVertices.remove(firstAvailable)
      while let topVertexId = lifo.popLast() {
        for neigborhEdgeVertex in outgoingNeighbors(of: topVertexId) {
          if visited.contains(neigborhEdgeVertex.vertex.id) {
            // Cycle found
            return false
          } else {
            let neighborhId = neigborhEdgeVertex.vertex.id
            visited.insert(neighborhId)
            lifo.append(neighborhId)
            uncheckedVertices.remove(neighborhId)
          }
        }
      }
    }
    return true
  }
  public var connected: Bool {
    if let startVertex = self.diVertices.first?.key {
      var visitedVerticesCnt = 0
      depthFirstSearch(from: startVertex, callback: { _ in visitedVerticesCnt += 1 })
      return visitedVerticesCnt == self.numberOfVertices
    } else {
      return true // Empty graph.
    }
  }
  public var isSymmetric : Bool {
    for (_,diEdge) in diEdges {
      if findEdge(from: diEdge.end, to: diEdge.start).isEmpty {
        return false
      }
    }
    return true
  }
}

//
//  dijkstra.swift
//  SwiftMath
//
//  Created by Pavel R on 12/06/2018.
//

import Foundation

public enum GraphSearch {
  /// Dijsktra algorithm for finding a shortest path.
  ///
  /// - Parameters:
  ///     - graph: The graph where we are searching the shortest path.
  ///     - sourceId: Id of the source vertex
  ///     - pathTo: Id of the vertices to which we want to find the shortest paths.
  ///     - lengths: A function that takes id of an edge as a parameter and returns the length of the edge.
  /// - Returns:
  ///     - distances: The lengths of the shortest paths to all reachable vertices.
  ///     - paths: The paths to the vertices specified by the parameter pathTo. Each path is sequence of tuples (edgeTo, vertex). So the path is sourceId,(edge,vertex),(edge,vertex)...(edge,destinationVertex).
  public static func findShortestPathsDijkstra<
    G: DiGraphProtocol,
    F: AdditiveArithmetic & Comparable>
  (
    in graph: G,
    sourceId: G.V.Index,
    pathTo: [G.V.Index],
    lengths: @escaping (G.E.Index) -> F,
    distancesTo: Set<G.V.Index>? = nil) -> (
    distances: [G.V.Index: F],
    paths: [G.V.Index:[(edge: G.E.Index, vertex: G.V.Index)]])
  {
    
    var distancesToSet = distancesTo
    var closedSed = Set<G.V.Index>()
    var distances = [G.V.Index: F]()
    var edgeToPredecesor = [G.V.Index:G.E.Index]()
    var openSet = OpenMinSet<G.V.Index, F>() //
    
    func computeDistances() {
      openSet.push(sourceId, score: F.zero)
      distances[sourceId] = F.zero
      while let vertexId = openSet.pop() {
        closedSed.insert(vertexId)
        distancesToSet?.remove(vertexId)
        if distancesToSet?.isEmpty ?? false {
          return
        }
        
        for neighborEdgeVertex in graph.outgoingNeighbors(of: vertexId) {
          let outEdge = neighborEdgeVertex.edge
          let neighbourId = outEdge.end
          let length = lengths(outEdge.id)
          assert(length >= F.zero)
          if !closedSed.contains(neighbourId) {
            let newDistance = distances[vertexId]! + length
            if distances[neighbourId] == nil || newDistance < distances[neighbourId]! {
              distances[neighbourId] = newDistance
              edgeToPredecesor[neighbourId] = outEdge.id
              if openSet.contains(neighbourId) {
                openSet.decreaseKey(neighbourId, to: newDistance)
              } else {
                openSet.push(neighbourId, score: newDistance)
              }
            }
          }
        }
      }
    }
    
    computeDistances()
    
    // path is a sequence of vertices from sourceId to v in pathTo
    var paths = [G.V.Index:[(edge: G.E.Index, vertex: G.V.Index)]]()
    for v in pathTo {
      assert(distancesTo?.contains(v) ?? true)
      var current = v
      var path = [(edge: G.E.Index, vertex: G.V.Index)]()
      while let edge = edgeToPredecesor[current] {
        path.append((edge:edge,vertex:current))
        current = graph.diEdge(edge)!.start
      }
      path.reverse()
      paths[v] = path
    }
    return (distances: distances, paths: paths)
  }
  
  
  public static func findShortestPathAStar<G: DiGraphProtocol>(
    in graph: G,
    sourceId: G.V.Index,
    pathTo: G.V.Index,
    lengths: @escaping (G.E.Index) -> Double,
    heuristics: @escaping (G.V.Index) -> Double) -> (
    distance: Double?,
    path: [(edge: G.E.Index, vertex: G.V.Index)]?)
  {
    
    var closedSet = Set<G.V.Index>()
    //var fScore = [G.V.Index:Double]()
    var gScore = [G.V.Index:Double]()
    var edgeToPredecesor = [G.V.Index:G.E.Index]()
    var openSet = OpenMinSet<G.V.Index, Double>()
    //PriorityQueue<DistanceVertexId<G.V.Index>>()
    //var orderId = 0
    
    func computeDistances() {
      gScore[sourceId] = 0.0
      let initNodeFScore = heuristics(sourceId)
      //fScore[sourceId] = initNodeFScore
      openSet.push(sourceId, score: initNodeFScore)
      //openSet.push(DistanceVertexId(dist: initNodeFScore, vertexId: sourceId, orderId: orderId))
      //orderId += 1
      while let vertexId = openSet.pop() {
        //let vertexId = minFScoreVertex.vertexId
        if vertexId == pathTo {
          return
        }
        closedSet.insert(vertexId)
        
        for neighborEdgeVertex in graph.outgoingNeighbors(of: vertexId) {
          let outEdge = neighborEdgeVertex.edge
          let neighborId = outEdge.end
          let length = lengths(outEdge.id)
          if !closedSet.contains(neighborId) {
            let tentativeGScore = gScore[vertexId]! + length
            assert(tentativeGScore >= 0.0)
            if tentativeGScore < gScore[neighborId] ?? Double.infinity {
              gScore[neighborId] = tentativeGScore
              edgeToPredecesor[neighborId] = outEdge.id
              let newFScore = tentativeGScore + heuristics(neighborId)
              if openSet.contains(neighborId) {
                openSet.decreaseKey(neighborId, to: newFScore)
              } else {
                openSet.push(neighborId, score: newFScore)
              }
            }
          }
        }
      }
    }
    
    computeDistances()
    
    if let finalCost = gScore[pathTo] {
      
      // path is a sequence of vertices from sourceId to v in pathTo
      var current = pathTo
      var path = [(edge:G.E.Index,vertex:G.V.Index)]()
      while let edge = edgeToPredecesor[current] {
        path.append((edge:edge,vertex:current))
        current = graph.diEdge(edge)!.start
      }
      //path.append(current)
      path.reverse()
      
      return (distance: finalCost, path: path)
    } else {
      return (nil,nil)
    }
  }
  
  
}

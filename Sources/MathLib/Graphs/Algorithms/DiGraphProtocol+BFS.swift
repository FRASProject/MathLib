//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/10/21.
//

import Foundation

extension DiGraphProtocol {
  public func breadthFirstSearch(
    from startId: V.Index,
    callback: (V) -> Bool,
    maxDepth : Int? = nil)
  {
    var fifo = Queue<V.Index>()
    var visited = Set<V.Index>()
    var hops = [V.Index:Int]()
    
    visited.insert(startId)
    fifo.enqueue(startId)
    hops[startId] = 0
    if !callback(diVertex(startId)!) {
      return
    }
    
    while let firstVertexId = fifo.dequeue() {
      if hops[firstVertexId]! < maxDepth ?? Int.max {
        for neigborhId in outgoingNeighbors(of: firstVertexId) where
          !visited.contains(neigborhId.vertex.id)
        {
          visited.insert(neigborhId.vertex.id)
          fifo.enqueue(neigborhId.vertex.id)
          hops[neigborhId.vertex.id] = hops[firstVertexId]! + 1
          if !callback(diVertex(neigborhId.vertex.id)!) {
            return
          }
        }
      }
    }
  }
  
  public func getVerticesId(withHopslessOrEqual hops: Int, from vertexId: V.Index) -> Set<V.Index> {
    var verticesId = Set<V.Index>()
    verticesId.insert(vertexId)
    let callback = { (vertex: V) -> Bool in
      verticesId.insert(vertex.id)
      return true
    }
    breadthFirstSearch(from: vertexId, callback: callback, maxDepth: hops)
    return verticesId
  }
}

//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/11/21.
//

import Foundation

extension FiniteGraph {
  public func findSomeAcyclicOrientation() -> DiGraph<V, IntDiEdge> where V.Index == Int, E.Index == Int {
    var orientation =  DiGraph<V, IntDiEdge>(isolatedVertices: vertices.lazy.map {$0.value})
    for (_, edge) in edges where !edge.isLoop {
      let startId = min(edge.vertex1Id, edge.vertex2Id)
      let endId = max(edge.vertex1Id, edge.vertex2Id)
      orientation.add(edge: IntDiEdge(
        id: edge.id,
        start: startId,
        end: endId))
    }
    return orientation
  }
}

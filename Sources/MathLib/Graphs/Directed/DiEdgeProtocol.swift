//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/10/21.
//

import Foundation

public protocol DiEdgeProtocol {
  associatedtype Index: Hashable
  associatedtype VertexIndex : Hashable
  
  var id : Index { get }
  var start : VertexIndex { get }
  var end : VertexIndex { get }
}

extension DiEdgeProtocol {
  public var isLoop : Bool {
    return start == end
  }
}

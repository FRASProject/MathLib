//
//  diGraph.swift
//  SwiftMath
//
//  Created by Pavel Rytir on 6/17/18.
//

import Foundation

public protocol DiGraphProtocol {
  associatedtype V: VertexProtocol
  associatedtype E: DiEdgeProtocol where E.VertexIndex == V.Index
  associatedtype NeighborsCollection : Sequence where NeighborsCollection.Element == (edge: E, vertex: V)
  associatedtype NeighborsIdCollection : Sequence where NeighborsIdCollection.Element == (edgeId: E.Index, vertexId: V.Index)
  
  func outgoingNeighbors(of vertexId: V.Index) -> NeighborsCollection
  func incomingNeighbors(of vertexId: V.Index) -> NeighborsCollection
  func outgoingNeighborsId(of vertexId: V.Index) -> NeighborsIdCollection
  func incomingNeighborsId(of vertexId: V.Index) -> NeighborsIdCollection
  func diVertex(_ id: V.Index) -> V?
  func diEdge(_ id: E.Index) -> E?
}

extension DiGraphProtocol {
  public func findEdge(from start: V.Index, to end: V.Index) -> [E] {
    let neighbors = outgoingNeighborsId(of: start)
    let edges = neighbors.filter {$0.vertexId == end } .map { diEdge($0.edgeId)! }
    return edges
  }
  
  public func isEdge(from start: V.Index, to end: V.Index) -> Bool {
    return !findEdge(from: start, to: end).isEmpty
  }
  
  @available(*, deprecated)
  public func findEdge(from start: V, to end: V) -> [E] {
    return findEdge(from: start.id, to: end.id)
  }
}

//
//  File.swift
//  
//
//  Created by Pavel Rytir on 8/12/22.
//

import Foundation

public struct SimpleDiEdge<V: Hashable>: DiEdgeProtocol {
  public typealias Index = Tuple<V,V>
  public typealias VertexIndex = V
  public let start: V
  public let end: V
  public var id: Tuple<V,V> {
    Tuple(start, end)
  }
  public init(start: V, end: V) {
    self.start = start
    self.end = end
  }
}

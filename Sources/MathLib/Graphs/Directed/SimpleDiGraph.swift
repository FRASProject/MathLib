//
//  File.swift
//  
//
//  Created by Pavel Rytir on 8/12/22.
//

import Foundation

public typealias SimpleDiGraph<HC: Hashable & Comparable> = DiGraph<SimpleVertex<HC>, SimpleDiEdge<HC>>

extension SimpleDiGraph {
  public init<HC: Hashable & Comparable>(vertices: [HC], edges: [(HC,HC)]) where
  V == SimpleVertex<HC>, E == SimpleDiEdge<HC>
  {
    let graphVertices = vertices.map {(key: $0, value: SimpleVertex(id: $0))}
    let graphEdges = edges.map { SimpleDiEdge(start: $0.0, end: $0.1)}.map {(key: $0.id, value: $0)}
    self = SimpleDiGraph<HC>(diVertices: graphVertices, diEdges: graphEdges)
  }
}

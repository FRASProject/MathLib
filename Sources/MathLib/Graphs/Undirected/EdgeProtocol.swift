//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/11/21.
//

import Foundation

public protocol EdgeProtocol {
  associatedtype Index: Hashable & Comparable
  associatedtype VertexIndex: Hashable & Comparable
  var id: Index { get }
  var vertices: TwoSet<VertexIndex> { get }
}

extension EdgeProtocol {
  public var vertex1Id: VertexIndex {
    return vertices.e1
  }
  public var vertex2Id: VertexIndex {
    return vertices.e2
  }
  public var isLoop : Bool {
    return vertex1Id == vertex2Id
  }
}

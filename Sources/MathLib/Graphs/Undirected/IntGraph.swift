//
//  IntGraph.swift
//  MathLib
//
//  Created by Pavel Rytir on 7/5/20.
//

import Foundation

public typealias IntGraph = Graph<IntVertex,IntEdge>

public struct IntVertex : VertexProtocol, Hashable {
  public typealias Index = Int
  public let id: Index
  public init(_ id: Index) {
    self.id = id
  }
  public init(id: Index) {
    self.id = id
  }
}

public struct IntEdge : EdgeProtocol, Hashable {
  public typealias Index = Int
  public typealias VertexIndex = Int
  
  public let id: Index
  public var vertices: TwoSet<Int>
}

extension IntGraph {
  public init(isolatedVertices: [Int]) {
    self = IntGraph(isolatedVertices: isolatedVertices.map {IntVertex(id: $0)})
  }
}

//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/11/21.
//

import Foundation

public protocol MutableGraph : FiniteGraph {
  mutating func add(edge: E)
  mutating func add(vertex: V)
  mutating func remove(edge: E)
  mutating func remove(vertex: V)
}

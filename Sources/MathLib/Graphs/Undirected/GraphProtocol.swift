//
//  graph.swift
//  SwiftMath
//
//  Created by Pavel R on 12/06/2018.
//

import Foundation

public protocol GraphProtocol {
  associatedtype V: VertexProtocol
  associatedtype E: EdgeProtocol where E.VertexIndex == V.Index
  associatedtype NeighborsCollection: Sequence where
    NeighborsCollection.Element == (edge: E, vertex: V)
  func neighbors(of vertexId: V.Index) -> NeighborsCollection
  func vertex(_ id: V.Index) -> V?
  func edge(_ id: E.Index) -> E?
}

extension GraphProtocol {
  public func isEdge(_ v1: V, _ v2: V) -> Bool {
    isEdge(v1.id, v2.id)
  }
  public func isEdge(_ v1: V.Index, _ v2: V.Index) -> Bool {
    let v1Neighbors = neighbors(of: v1)
    return v1Neighbors.first(where: {$0.vertex.id == v2}) != nil
  }
}

//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/11/21.
//

import Foundation

public protocol FiniteGraph: GraphProtocol {
  associatedtype VertexCollection: Collection where
    VertexCollection.Element == (key:V.Index, value: V)
  associatedtype EdgeCollection: Collection where EdgeCollection.Element == (key: E.Index, value: E)
  
  var vertices: VertexCollection { get }
  var edges: EdgeCollection { get }
  var numberOfVertices: Int { get }
  var numberOfEdges: Int { get }
}

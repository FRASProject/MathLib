//
//  File.swift
//  
//
//  Created by Pavel Rytir on 8/12/22.
//

import Foundation

public struct SimpleVertex<V: Hashable & Comparable>: VertexProtocol {
  public var id: V
  public typealias Index = V
}

//
//  simpleGraph.swift
//  SwiftMath
//
//  Created by Pavel R on 13/06/2018.
//

import Foundation


public struct Graph<V: VertexProtocol, E: EdgeProtocol>: FiniteGraph where
  V.Index == E.VertexIndex
{
  public typealias VertexCollection = [V.Index:V]
  public typealias EdgeCollection = [E.Index:E]
  public typealias NeighborsCollection = [(edge: E, vertex: V)]
  
  public internal(set) var vertices: VertexCollection
  public internal(set) var edges: EdgeCollection
  internal var neighbors : [V.Index: [NeighborTuple<V.Index,E.Index>]]
  
  /// Creates an empty graph.
  public init() {
    self.vertices = [:]
    self.edges = [:]
    self.neighbors = [:]
  }
  
  public func neighbors(of vertexId: V.Index) -> NeighborsCollection {
    return (neighbors[vertexId] ?? []).map {(edge: edge($0.edgeId)!, vertex: vertex($0.vertexId)!)}
  }
  
  public var numberOfVertices : Int {
    return vertices.count
  }
  
  public var numberOfEdges : Int {
    return edges.count
  }
  
  public func vertex(_ id: V.Index) -> V? {
    return vertices[id]
  }
  
  public func edge(_ id: E.Index) -> E? {
    return edges[id]
  }
  public init(isolatedVertices vertices: [V]) {
    self.vertices = Dictionary(uniqueKeysWithValues: vertices.lazy.map {($0.id, $0)})
    self.edges = [:]
    self.neighbors = [:]
  }
}

struct NeighborTuple<VertexIndex: Hashable, EdgeIndex: Hashable> : Hashable {
  public let vertexId : VertexIndex
  public let edgeId : EdgeIndex
}


extension Graph: Equatable where V: Equatable, E: Equatable {}

extension Graph: Hashable where V: Hashable, E:Hashable {}

extension NeighborTuple: Codable where VertexIndex : Codable, EdgeIndex : Codable {}

extension Graph: Codable where V: Codable, E: Codable, V.Index : Codable, E.Index: Codable {}

extension Graph {
  
  // Will usualy result in multigraph.
  public init<DG: FiniteDiGraph>(diGraph: DG) where DG.V == V, DG.E == E {
    // , uniquingEdgesWith: (E,E) -> E
    self.vertices = Dictionary(uniqueKeysWithValues: Array(diGraph.diVertices))
    self.edges = Dictionary(uniqueKeysWithValues: Array(diGraph.diEdges))
    self.neighbors = [:]
    
    for (vertexId, _) in vertices {
      let outgoingNeighbors = diGraph.outgoingNeighbors(of: vertexId).map {NeighborTuple(vertexId: $0.vertex.id, edgeId: $0.edge.id)}
      let incomingNeighbors = diGraph.incomingNeighbors(of: vertexId).map {NeighborTuple(vertexId: $0.vertex.id, edgeId: $0.edge.id)}
      var vertexNeighbors = outgoingNeighbors
      vertexNeighbors.append(contentsOf: incomingNeighbors)
      self.neighbors[vertexId] = vertexNeighbors
    }
  }
  
  public init<G: FiniteGraph>(graph: G, inducedOn verticesIds: Set<V.Index>) where G.V == V, G.E == E {
    
    self.vertices = Dictionary(uniqueKeysWithValues: verticesIds.map { (vertexId: V.Index) -> (V.Index,V) in
      let vertex = graph.vertex(vertexId)!
      return (vertex.id, vertex)
    })
    self.neighbors = [:]
    
    self.edges = [E.Index: E]()
    
    for (vertexId, _) in vertices {
      
      let vertexNeighbors = graph.neighbors(of: vertexId).filter { verticesIds.contains($0.vertex.id) }
      for neighbor in vertexNeighbors where edges[neighbor.edge.id] == nil {
        edges[neighbor.edge.id] = neighbor.edge
      }
      neighbors[vertexId] = vertexNeighbors.map { NeighborTuple(vertexId: $0.vertex.id, edgeId: $0.edge.id) }
    }
  }
  
  public init<G: FiniteGraph>(graph: G, onlyWithEdges edgesIds: Set<E.Index>) where G.V == V, G.E == E {
    
    
    self.edges = Dictionary(uniqueKeysWithValues: edgesIds.map { (key: $0, value: graph.edge($0)!) })
    
    var newVerticesIds = Set<V.Index>()
    
    for (_, edge) in edges {
      newVerticesIds.insert(edge.vertex1Id)
      newVerticesIds.insert(edge.vertex2Id)
    }
    
    self.vertices = Dictionary(uniqueKeysWithValues: newVerticesIds.map { ($0, graph.vertex($0)!) })
    self.neighbors = [:]
    
    for (vertexId, _) in vertices {
      let vertexNeighbors = graph.neighbors(of: vertexId).filter {edgesIds.contains($0.edge.id)}
      neighbors[vertexId] = vertexNeighbors.map { NeighborTuple(vertexId: $0.vertex.id, edgeId: $0.edge.id) }
    }
  }
}

extension Graph : MutableGraph {
  public mutating func add(vertex: V) {
    guard vertices[vertex.id] == nil else {
      fatalError("Graph already contains a vertex with the same id.")
    }
    vertices[vertex.id] = vertex
  }
  
  public mutating func remove(vertex: V) {
    assert(vertices[vertex.id] != nil, "Removing non-existing vertex.")
    let vertexNeighbors = neighbors(of: vertex.id)
    for neighbor in vertexNeighbors {
      remove(edge: neighbor.edge)
    }
    vertices.removeValue(forKey: vertex.id)
    neighbors.removeValue(forKey: vertex.id)
  }
  
  public mutating func add(edge: E) {
    guard edges[edge.id] == nil else {
      fatalError("Graph already contains an edge with the same id.")
    }
    attachEdge(to: edge.vertex1Id, edgeId: edge.id, neighborId: edge.vertex2Id)
    if edge.vertex1Id != edge.vertex2Id {
      attachEdge(to: edge.vertex2Id, edgeId: edge.id, neighborId: edge.vertex1Id)
    }
    edges[edge.id] = edge
  }
  public mutating func remove(edge: E) {
    assert(edges[edge.id] != nil, "Removing non-existing edge.")
    removeEdge(from: edge.vertex1Id, edgeId: edge.id)
    if edge.vertex1Id != edge.vertex2Id {
      removeEdge(from: edge.vertex2Id, edgeId: edge.id)
    }
    edges.removeValue(forKey: edge.id)
  }
  
  private mutating func attachEdge(to vertexId: V.Index, edgeId: E.Index, neighborId: V.Index) {
    assert(vertices[vertexId] != nil)
    assert(vertices[neighborId] != nil)
    neighbors[vertexId, default: []].append(NeighborTuple(vertexId: neighborId, edgeId: edgeId))
  }
  private mutating func removeEdge(from vertexId: V.Index, edgeId: E.Index) {
    let vertexNeighbors = neighbors[vertexId]!.filter {$0.edgeId != edgeId}
    neighbors[vertexId] = vertexNeighbors
  }
  private mutating func filterNeighbors(of vertexId: V.Index, _ isIncluded: (NeighborTuple<V.Index, E.Index>) -> Bool) {
    let vertexNeighbors = (neighbors[vertexId] ?? []).filter { isIncluded($0) }
    neighbors[vertexId] = vertexNeighbors
  }
}


extension Graph where Graph.V == IntVertex, Graph.E == IntEdge {
  public mutating func addNewVertex<Generator: IndexGenerator>(
    indexGenerator: inout Generator) -> V where Generator.Index == Int
  {
    let vertexId = indexGenerator.next()
    let newVertex = V(id: vertexId)
    self.add(vertex: newVertex)
    return newVertex
  }
  public mutating func addEdge<Generator: IndexGenerator>(
    v1: Int,
    v2: Int,
    indexGenerator: inout Generator) -> E where Generator.Index == Int
  {
    let edgeId = indexGenerator.next()
    let newEdge = E(id: edgeId, vertices: TwoSet(v1, v2))
    self.edges[edgeId] = newEdge
    attachEdge(to: v1, edgeId: edgeId, neighborId: v2)
    if v1 != v2 {
      attachEdge(to: v2, edgeId: edgeId, neighborId: v1)
    }
    return newEdge
  }
}

//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/11/21.
//

import Foundation

public protocol VertexProtocol {
  associatedtype Index: Hashable & Comparable
  var id: Index { get }
}


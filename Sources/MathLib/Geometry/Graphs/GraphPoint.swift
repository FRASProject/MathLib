//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/28/23.
//

import Foundation

public struct GraphPoint<T> : VertexProtocol, AbstractPoint  {
  public let id: Int
  public let coordinates: [T]
  public init(id: Int, coordinates: [T]) {
    self.id = id
    self.coordinates = coordinates
  }
  
  public var description : String {
    "(" + coordinates.map {"\($0)"}.joined(by: ",") + ")"
  }
}

extension GraphPoint: Codable where T: Codable {}



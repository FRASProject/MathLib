//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/29/23.
//

import Foundation

public enum GeometryGraphGenerator {
  public static func createGridGraph<T: Numeric>(
    spacing: [T],
    dimensions: [Int]) -> (GeometryGraph<T>, indexer: ([Int]) -> Int)
  {
    let vertexMaker = { (coordinates: [Int], id: Int) -> GraphPoint<T> in
      GraphPoint<T>(
        id: id,
        coordinates: zip(spacing, coordinates).map {$0 * T(exactly: $1)!})
    }
    var segmentEdgeIdGenerator = IntIndexGenerator()
    
    let segmentEdgeMaker = { (coordinates: [Int], vertex1Id: Int, coordinates2: [Int], vertex2Id: Int) -> GraphSegment in
      let diff = VectorUtils.subtractArrays(a: coordinates, b: coordinates2)
      let nonzereCoordinate = diff.firstIndex(where: {$0 != 0})!
      let length = spacing[nonzereCoordinate]
      return GraphSegment(
        id: segmentEdgeIdGenerator.next(),
        start: vertex1Id,
        end: vertex2Id,
        length: length)
    }
    
    let (graph, indexer) = GeneratorDiGraphs.createDiGrid(
      dimensions: dimensions,
      vertexMaker: vertexMaker,
      edgeMaker: segmentEdgeMaker,
      initialGraph: GeometryGraph<T>())

    let genId = { (x:Int, y: Int, z: Int) -> Int in
      return indexer([x,y,z])
    }
    
    assert(graph.isSymmetric)
    assert(graph.parallelEdges.isEmpty, "Find a multi-edge. There should not be multi-edges.") // There should not be multi-edges.
    
    return (graph, indexer)
  }
}

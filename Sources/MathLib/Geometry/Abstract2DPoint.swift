//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/28/23.
//

import Foundation

public protocol Abstract2DPoint : Hashable {
  var x : Double { get }
  var y : Double { get }
}

extension Abstract2DPoint {
  public func euclideanDistance<P: Abstract2DPoint>(to other: P) -> Double {
    return sqrt(pow(self.x - other.x, 2) + pow(self.y - other.y, 2))
  }
}

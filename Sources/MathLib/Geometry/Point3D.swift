//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/28/23.
//

import Foundation

public struct Point3D : Abstract3DPoint {
  public init(x: Double, y: Double, z: Double) {
    self.x = x
    self.y = y
    self.z = z
  }
  public let x : Double
  public let y : Double
  public let z : Double
}

extension Point3D {
  private func conv(_ a: Double, _ b: Double, _ t: Double) -> Double {
    return a * (1 - t) + t * b
  }
  public func convexCombination(with other: Point3D, t: Double) -> Point3D {
    return Point3D(x: conv(self.x, other.x, t), y: conv(self.y, other.y, t), z: conv(self.z, other.z, t))
  }
  
}

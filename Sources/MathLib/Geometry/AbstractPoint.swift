//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/29/23.
//

import Foundation

public protocol AbstractPoint<T> {
  associatedtype T
  var coordinates: [T] { get }
}

extension AbstractPoint {
  public var dimension: Int {
    coordinates.count
  }
}

extension AbstractPoint where T: AdditiveArithmetic {
  public func vectorTo<P: AbstractPoint>(_ other: P) -> [T] where T == P.T {
    precondition(self.dimension == other.dimension)
    return zip(self.coordinates, other.coordinates).map {$1 - $0}
  }
}

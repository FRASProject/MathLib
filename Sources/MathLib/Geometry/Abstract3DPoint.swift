//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/28/23.
//

import Foundation

public protocol Abstract3DPoint : Abstract2DPoint {
  var z : Double { get }
}

extension Abstract3DPoint {
  public func euclideanDistance<P: Abstract3DPoint>(to other: P) -> Double {
    return sqrt(pow(self.x - other.x, 2) + pow(self.y - other.y, 2) + pow(self.z - other.z, 2))
  }
  
  public func findIndexOfClosestPoint<P: Abstract3DPoint>(from points: [P]) -> Int {
    assert(!points.isEmpty)
    var minDist = Double.infinity
    var index = -1
    for pointIndex in points.indices {
      let distance = self.euclideanDistance(to: points[pointIndex])
      if distance < minDist {
        minDist = distance
        index = pointIndex
      }
    }
    return index
  }
}

public func splitSet<P: Abstract3DPoint>(_ set: [P], toClosests points: [P]) -> [P: [P]] {
  let intermediateDict = Dictionary(
    grouping: set.map { ($0, points[$0.findIndexOfClosestPoint(from: points)] )},
    by: {$0.1})
  return intermediateDict.mapValues { $0.map { $0.0 } }
}
